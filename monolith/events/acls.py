from .keys import PEXELS_API_KEY , OPEN_WEATHER_API_KEY
import requests

def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY,
    }

    params = {
        "query": f"{city} {state} city",
        "per_page": 1,
    }

    response = requests.get("https://api.pexels.com/v1/search", headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()

        if "photos" in data and data["photos"]:
            photo_url = data["photos"][0]["src"]["original"]
            return {"picture_url": photo_url}
        else:
            return {"picture_url": None}
    else:
        print(f"Error: {response.status_code} - {response.text}")
        return {"error": f"Failed to fetch photo for {city}, {state}"}

def get_weather_data(city, state):
    geo_params = {
        "address": f"{city},{state} USA",
        "key": OPEN_WEATHER_API_KEY,
    }

    geo_response = requests.get("https://maps.googleapis.com/maps/api/geocode/json", params=geo_params)

    if geo_response.status_code == 200:
        geo_data = geo_response.json()
        results = geo_data.get("results", [])

        if results:
            location = results[0].get("geometry", {}).get("location", {})

            weather_params = {
                "lat": location.get("lat"),
                "lon": location.get("lng"),
                "units": "imperial",
                "appid": OPEN_WEATHER_API_KEY,
            }

            weather_response = requests.get("https://api.openweathermap.org/data/2.5/weather", params=weather_params)

            if weather_response.status_code == 200:
                weather_data = weather_response.json()
                temp = weather_data.get("main", {}).get("temp")
                description = weather_data.get("weather", [])[0].get("description")

                return {"temp": temp, "description": description}
            else:
                print(f"Error fetching weather data: {weather_response.status_code} - {weather_response.text}")
                return {"weather": None}
        else:
            print(f"No results found for geocoding API. Response: {geo_data}")
            return {"weather": None}
    else:
        print(f"Error fetching geocoding data: {geo_response.status_code} - {geo_response.text}")
        return {"weather": None}
